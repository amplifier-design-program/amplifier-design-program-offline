Amplifier Design Program 

Calculate:
 1) Stability Circle    
 2) Unilateral Figure of Merit    
 3) Unilateral Power Gain Circles    
 4) Simultaneous Conjugate Match    
 5) Power & Available Power Gain Circles    
 6) Noise Figure Circles    
 7) Input/Output Reflection Coefficient    
 8) Conversion between Γ & Z    